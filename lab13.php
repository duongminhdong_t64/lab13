<style>
<?php include 'lab13.css';
?>
</style>
<?php
$khoa = array(
    '' => '',
    'MAT' => 'Khoa học máy tính',
    'KDL' => 'khoa học vật liệu'
);
if (!isset($_POST['faculty'])) {
    $_POST['faculty'] = '';
}

session_start();
if (!isset($_SESSION['count_student'])) {
    $_SESSION['count_student'] = 0;
}
?>
<script>
window.onload = function() {

    var faculty = localStorage.getItem("faculty");
    var keyWord = localStorage.getItem("keyWord");
    const collectElement = document.getElementsByClassName("input1");
    collectElement[0].value = faculty;
    collectElement[1].value = keyWord;

}

function deleteIndexValue(a) {
    const collectElement = document.getElementsByClassName(a);
    for (let i = 0; i < collectElement.length; i++) {
        collectElement[i].value = "";
    }
    localStorage.removeItem("faculty");
    localStorage.removeItem("keyWord");
}

function saveData(a) {
    collectElement = document.getElementsByClassName(a);
    localStorage.setItem("faculty", collectElement[0].value);
    localStorage.setItem("keyWord", collectElement[1].value);
}
</script>
<html>

<head>
    <title>Trang đăng nhập</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
</head>

<body>
    <div class="mx-auto mt-5 border d-flex flex-column" id="container" style="max-width: 80% !important  ">
        <div class="my-3 d-flex flex-column mx-auto">
            <form method="POST">
                <div class="d-flex mb-2" style="height: 35px">
                    <label class="input-label h-100 mr-3">Khoa<span> *</span></label>
                    <select class="input1" name="faculty" style="width: 189px; border: 1px solid #2980d7">
                        <option></option>
                        <?php foreach ($khoa as $i =>
                            $i) : ?>
                        <option><?php echo $khoa[$i] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <br /><br />
                </div>
                <div class="d-flex mb-2" style="height: 35px">
                    <label class="input-label h-100 mr-3">Từ khóa</label>
                    <input class="h-100 input1" type="text" style="border: 1px solid #2980d7" name="key">
                </div>
                <div class="d-flex">
                    <button onclick="deleteIndexValue('input1')"
                        class="btn1 mx-auto btn btn-primary btn-submit mt-3">Xóa</button>
                    <button onclick="saveData('input1');" class="mx-auto btn btn-primary btn-submit mt-3">Tìm
                        kiếm</button>
                </div>
            </form>
        </div>
        <div class="d-flex justify-content-between mx-2">
            <span style="font-weight: 500">Số sinh viên tìm thấy : <?php echo $_SESSION['count_student'];   ?> > </span>
            <button onclick="window.location='labform13.php'" class="btn btn-primary btn-add">Thêm</button>
        </div>
        <div class="mt-4">
            <table className="padding-table-columns">
                <tr>
                    <th>Id</th>
                    <th>Tên sinh viên</th>
                    <th>Giới Tính</th>
                    <th> Khoa</th>
                    <th> Địa chỉ</th>
                    <th>Action</th>
                </tr>
                <?php

                $servername = "localhost";
                $username = "root";
                $password = "";
                $dbname = "php_validate";

                // Create connection
                $conn = mysqli_connect($servername, $username, $password, $dbname);
                // Check connection
                if (!$conn) {
                    die("Connection failed: " . mysqli_connect_error());
                }
                if ($_POST['faculty'] === '' & $_POST['key'] === '') {
                    $sql = "SELECT * FROM student";
                } else if ($_POST['faculty'] === 'Khoa học máy tính' & $_POST['key'] === '') {
                    $sql = "SELECT * FROM student WHERE faculty = 'MAT' ";
                } else {
                    if ($_POST['key'] === '') {
                        $sql = "SELECT * FROM student WHERE faculty = 'KDL' ";
                    } else {
                        $sql = "SELECT * FROM student WHERE name LIKE '%{$_POST['key']}%' OR address LIKE '%{$_POST['key']}%' ";
                    }
                }

                $result = mysqli_query($conn, $sql);

                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    $_SESSION['count_student'] = 0;
                    while ($row = mysqli_fetch_assoc($result)) {
                        $_SESSION['count_student']++;
                        // echo "id: " . $row["id"] . " - Name: " . $row["name"] . " " . $row["gender"] . "<br>";
                ?>
                <tr>
                    <td>

                        <?php echo $row["id"] ?>

                    </td>
                    <td>
                        <?php echo $row["name"] ?>
                    </td>
                    <td>
                        <?php echo $row['gender'] === '0' ? 'Nam' : 'Nữ'  ?> </td>
                    <td>
                    <td>
                        <?php echo  trim($khoa[$row['faculty']], "  ");  ?> </td>
                    <td>
                    <td>
                        <?php echo $row['address'] ?> </td>
                    <td>
                        <div>
                            <button class="btn btn-action">Xóa</button>
                            <button class="btn btn-action">Sửa</button>
                        </div>
                    </td>
                </tr>

                <?php
                    }
                }

                mysqli_close($conn);

                ?>
            </table>
        </div>
    </div>
</body>

</html>